/**
 * 
 */
package com.lcw.demo.t9;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.lcw.comm.fragment.ApproveFragment;
import com.lcw.comm.fragment.ApproveFragment.CallBackListener;
import com.lcw.comm.fragment.KeyBoardUtil;
import com.lcw.demot9.R;

/**
 * TODO
 * @author lcw 2014-4-9 ����4:06:52
 */
public class MainActivity extends Activity{
	private ApproveFragment fragment;
	public EditText	search_et;
	private Button removeBtn;
	private Button addBtn;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		search_et = (EditText)findViewById(R.id.search_et);
		fragment=(ApproveFragment) KeyBoardUtil.addT9KeyboardFragement(this,search_et,R.id.t9_lty);
//		fragment=addApproveFragement(search_et,R.id.t9_lty);
		removeBtn=(Button) findViewById(R.id.remove_fr_btn);
		addBtn=(Button) findViewById(R.id.add_fr_btn);
		removeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				KeyBoardUtil.removeFragment(MainActivity.this, fragment);
			}
		});
		addBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fragment=(ApproveFragment) KeyBoardUtil.addT9KeyboardFragement(MainActivity.this,search_et,R.id.t9_lty);
			}
		});
	}
	

}
