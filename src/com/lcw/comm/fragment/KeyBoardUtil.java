package com.lcw.comm.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.EditText;

import com.lcw.comm.fragment.ApproveFragment.CallBackListener;
/**
 * 键盘管理类
 * @author xixiyue
 *
 */
public class KeyBoardUtil {
	/**
	 * 展示T9键盘
	 * @param activity 调用T9键盘的界面
	 * @param view 用来输入内容的EditView控件
	 * @param parrentId 需要插入控件的ID
	 * @return
	 */
	public static Fragment addT9KeyboardFragement(Activity activity,final EditText view,int parrentId){
		FragmentManager fm = activity.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ApproveFragment fragment = new ApproveFragment(view);
		fragment.setCallBackListener(new CallBackListener() {

			@Override
			public void onDeleteClicked() {
//				Toast.makeText(MainActivity.this, "deleted a char", Toast.LENGTH_SHORT).show();
				int count = view.getText().length();
				if(count>0){
					int start  = count-1;
					int end  = count;
					view.getText().delete(start, end);
				}
			}

			@Override
			public void onDeleteLongClicked() {
//				Toast.makeText(MainActivity.this, "deleted Long clicked", Toast.LENGTH_SHORT).show();
				view.getText().clear();
				
			}

			@Override
			public void onClearClicked() {
//				Toast.makeText(MainActivity.this, "clear clicked", Toast.LENGTH_SHORT).show();
				view.getText().clear();
			}

			@Override
			public void onCharSelected(Character c) {
//				Toast.makeText(MainActivity.this, "append "+c, Toast.LENGTH_SHORT).show();
				view.getText().append(c);
			}
		});
		ft.replace(parrentId, fragment);
		ft.commit();
		return fragment;
	}
	/**
	 * 移除fragment
	 * @param activity要移除frament所在的Activity
	 * @param fragment 要移除的fragment
	 */
	public static void removeFragment(Activity activity,android.app.Fragment fragment){
		FragmentManager fm =activity.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.remove(fragment);
		ft.commit();
	}
}
