/**
 * 
 */
package com.lcw.comm.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lcw.demot9.R;

/**
 * TODO
 * @author lcw 2014-4-10 ����10:03:28
 */
public class T9View extends LinearLayout {

	private ImageView			imageView;
	
	/**
	 * @param context
	 * @param attrs
	 */
	public T9View(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray styles = context.obtainStyledAttributes(attrs, R.styleable.t9);
		Drawable style = styles.getDrawable(R.styleable.t9_img);
		init(context, style);
	}

	/**
	 * @param context
	 */
	public T9View(Context context) {
		super(context);
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public T9View(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	/**
	 * @param context
	 * @param style
	 */
	private void init(Context context, Drawable style) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View mPreview = inflater.inflate(R.layout.t9_item, this);
		ImageView mImgView = (ImageView) mPreview.findViewById(R.id.t9_item_imgv);
		if(mImgView!=null && style!=null){
			mImgView.setImageDrawable(style);
		}
	}

}
