/**
 * 
 */
package com.lcw.comm.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lcw.demot9.R;

/**
 * TODO 审批Fragment组件
 * @author lichunwen 2014-1-23 下午9:55:26
 */
public class ApproveFragment extends Fragment{
	private Activity _activity =null;
	private Button agree_btn;
	private Button refuse_btn;
	private Button hasView_btn;
	private CallBackListener callBackListener=null;
	private boolean initialed=false;
	private T9View dialNum1;
	private T9View dialNum2;
	private T9View dialNum3;
	private T9View dialNum4;
	private T9View dialNum5;
	private T9View dialNum6;
	private T9View dialNum7;
	private T9View dialNum8;
	private T9View dialNum9;
	private T9View dialNum10;
	private T9View dialNum11;
	private T9View dialNum12;
	private T9View dialNum13;
	private T9View dialNum14;
	InputMethodManager inputManager ;

	protected String	currentTypeTag="pinyin";//defaut type
	private FrameLayout	content_etv;
	
	private char[][] pinYingStrArr={ {'1'} 					,{'A','B','C','2'} ,{'D','E','F','3'}
									,{'G','H','I','4'} 		,{'J','K','L','5'} ,{'M','N','O','6'}
									,{'P','Q','R','S','7'} 	,{'T','U','V','8'} ,{'W','X','Y','Z','9'}
									,{}						,{'0'} 			   ,{}		
									};
	private char[][] biHua		={ {}		,{'一'}	,{}
								  ,{'丿'}	,{'丨'}	,{'丶'}
								  ,{}		,{'乛'}	,{}
			
	};
	private FrameLayout	item_dialog_framLyt;
	private TextView	left_tv;
	private TextView	right_tv;
	private TextView	up_tv;
	private TextView	down_tv;
	private TextView	center_tv;
	private EditText et;
	private View	showItemDialogBeforView; //record the focus of view befor show item dialog
	

	public ApproveFragment(EditText et) {
		// TODO Auto-generated constructor stub
		this.et=et;
	}
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_activity =activity;
		//TODO 获取主activity的引用;
		Log.d(ApproveFragment.class.getSimpleName(), "onAttach()");
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//TODO　创建后台工作线程和任务
		Log.d(ApproveFragment.class.getSimpleName(), "onCreate()");
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(ApproveFragment.class.getSimpleName(), "OnCreatView()");
		// TODO 创建自己的用户界面
		//创建、填充Fragment的UI，并返回它，
		//如果这个Fragment没有 UI，那么返回null

		View view =  inflater.inflate(R.layout.fragment_t9, container, false);
		
		content_etv = (FrameLayout) view.findViewById(R.id.content_etv);
		
//		t9_rd_btn = (RadioButton) view.findViewById(R.id.t9_rd_btn);

		
		item_dialog_framLyt = (FrameLayout) view.findViewById(R.id.item_dialog_framLyt);
		
		dialNum1 = (T9View) view.findViewById(R.id.dialNum1);
		dialNum2 = (T9View) view.findViewById(R.id.dialNum2);
		dialNum3 = (T9View) view.findViewById(R.id.dialNum3);
		dialNum4 = (T9View) view.findViewById(R.id.dialNum4);
		dialNum5 = (T9View) view.findViewById(R.id.dialNum5);
		dialNum6 = (T9View) view.findViewById(R.id.dialNum6);
		dialNum7 = (T9View) view.findViewById(R.id.dialNum7);
		dialNum8 = (T9View) view.findViewById(R.id.dialNum8);
		dialNum9 = (T9View) view.findViewById(R.id.dialNum9);
		dialNum10 = (T9View) view.findViewById(R.id.dialNum10);
		dialNum11 = (T9View) view.findViewById(R.id.dialNum11);
		dialNum12 = (T9View) view.findViewById(R.id.dialNum12);
		dialNum14 = (T9View) view.findViewById(R.id.dialNum14);
		dialNum1.requestFocus();
		dialNum1.setOnClickListener(onclickListener);
		dialNum2.setOnClickListener(onclickListener);
		dialNum3.setOnClickListener(onclickListener);
		dialNum4.setOnClickListener(onclickListener);
		dialNum5.setOnClickListener(onclickListener);
		dialNum6.setOnClickListener(onclickListener);
		dialNum7.setOnClickListener(onclickListener);
		dialNum8.setOnClickListener(onclickListener);
		dialNum9.setOnClickListener(onclickListener);
		dialNum10.setOnClickListener(onclickListener);
		dialNum11.setOnClickListener(onclickListener);
		dialNum12.setOnClickListener(onclickListener);
		dialNum14.setOnClickListener(onclickListener);
		dialNum12.setOnLongClickListener(onLongClickListener);
		T9View bihua_num1 = (T9View) view.findViewById(R.id.bihua_num1);
		T9View bihua_num2 = (T9View) view.findViewById(R.id.bihua_num2);
		T9View bihua_num3 = (T9View) view.findViewById(R.id.bihua_num3);
		T9View bihua_num4 = (T9View) view.findViewById(R.id.bihua_num4);
		T9View bihua_num5 = (T9View) view.findViewById(R.id.bihua_num5);
		T9View bihua_num6 = (T9View) view.findViewById(R.id.bihua_num6);
		T9View bihua_num7 = (T9View) view.findViewById(R.id.bihua_num7);
		T9View bihua_num8 = (T9View) view.findViewById(R.id.bihua_num8);
		T9View bihua_num9 = (T9View) view.findViewById(R.id.bihua_num9);
		bihua_num1.setOnClickListener(onclickListener);
		bihua_num2.setOnClickListener(onclickListener);
		bihua_num3.setOnClickListener(onclickListener);
		bihua_num4.setOnClickListener(onclickListener);
		bihua_num5.setOnClickListener(onclickListener);
		bihua_num6.setOnClickListener(onclickListener);
		bihua_num7.setOnClickListener(onclickListener);
		bihua_num8.setOnClickListener(onclickListener);
		bihua_num9.setOnClickListener(onclickListener);
		bihua_num9.setOnLongClickListener(onLongClickListener);
		
		up_tv = (TextView)item_dialog_framLyt.findViewById(R.id.up_tv);
		down_tv = (TextView)item_dialog_framLyt.findViewById(R.id.down_tv);
		center_tv = (TextView)item_dialog_framLyt.findViewById(R.id.center_tv);
		left_tv = (TextView)item_dialog_framLyt.findViewById(R.id.left_tv);
		right_tv = (TextView)item_dialog_framLyt.findViewById(R.id.right_tv);
		
		up_tv.setOnClickListener(onclickListener);
		down_tv.setOnClickListener(onclickListener);
		center_tv.setOnClickListener(onclickListener);
		left_tv.setOnClickListener(onclickListener);
		right_tv.setOnClickListener(onclickListener);
		
		up_tv.setOnKeyListener(onkeyListener);
		down_tv.setOnKeyListener(onkeyListener);
		center_tv.setOnKeyListener(onkeyListener);
		left_tv.setOnKeyListener(onkeyListener);
		right_tv.setOnKeyListener(onkeyListener);
		currentTypeTag="pinyin";
		showFramLayoutItemByIndex(0);
		inputManager= (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
		return view;
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//TODO 当父activity的onCreate()执行完成后调用,Fragment完成初始化后，进行操作
		Log.d(ApproveFragment.class.getSimpleName(), "onActivityCreate()");
		
	}
	/* (non-Javadoc)
	 * @see android.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		Log.d(ApproveFragment.class.getSimpleName(), "OnStart()");
	}
	
	/* (non-Javadoc)
	 * @see android.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		if(!initialed){
//			t9_rd_btn.setChecked(true);
//			initialed=true;
		}
		Log.d(ApproveFragment.class.getSimpleName(), "OnResume()");
	}
	
	/* (non-Javadoc)
	 * @see android.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		super.onPause();
		Log.d(ApproveFragment.class.getSimpleName(), "OnPause()");
	}
	
	
	/* (non-Javadoc)
	 * @see android.app.Fragment#onStop()
	 */
	@Override
	public void onStop() {
		super.onStop();
		Log.d(ApproveFragment.class.getSimpleName(), "OnStop()");
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroyView()
	 */
	@Override
	public void onDestroyView() {
		Log.d(ApproveFragment.class.getSimpleName(), "OnDestroyView()");
		// TODO 当消失到后台，进入堆栈时的回调
		//清除资源相关views
		super.onDestroyView();
	}
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		Log.d(ApproveFragment.class.getSimpleName(), "OnDestroy()");
		// TODO 当该Fragment销毁时的回调
		// 清除所有资源，包括后台执行线程，关闭数据库等
		super.onDestroy();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	@Override
	public void onDetach() {
		Log.d(ApproveFragment.class.getSimpleName(), "OnDetach()");
		// TODO 当该fragment 从activity上分离时，回调
		super.onDetach();
	}
	

	/**
	 * @return the callBackListener
	 */
	public CallBackListener getCallBackListener() {
		return callBackListener;
	}

	/**
	 * @param callBackListener the callBackListener to set
	 */
	public void setCallBackListener(CallBackListener callBackListener) {
		this.callBackListener = callBackListener;
	}

	View.OnLongClickListener onLongClickListener= new OnLongClickListener() {
		
		@Override
		public boolean onLongClick(View v) {
			
			switch (v.getId()) {
			case R.id.dialNum12:
			case R.id.bihua_num9:
				if(callBackListener!=null){
					callBackListener.onDeleteLongClicked();
				}
				break;
			default:
				break;
			}
			
			return true;
		}
	};
	View.OnKeyListener onkeyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
//			if(event.getAction()==KeyEvent.ACTION_DOWN){
//				
//				if(keyCode==KeyEvent.KEYCODE_DPAD_UP){
//					if(v.getId()==R.id.up_tv
//					  ||(v.getId()==R.id.center_tv && !up_tv.isEnabled())
//					  ||(v.getId()==R.id.down_tv && !center_tv.isEnabled() && !up_tv.isEnabled())
//					  ||(v.getId()==R.id.left_tv && !up_tv.isEnabled())
//					  ||(v.getId()==R.id.right_tv && !up_tv.isEnabled())
//					){
//						return true;
//					}
//				}else if(keyCode==KeyEvent.KEYCODE_DPAD_LEFT){
//					if(v.getId()==R.id.left_tv
//					  ||(v.getId()==R.id.center_tv && !left_tv.isEnabled())
//					  ||(v.getId()==R.id.right_tv && !center_tv.isEnabled() && !left_tv.isEnabled())
//					  ||(v.getId()==R.id.up_tv && !left_tv.isEnabled())
//					  ||(v.getId()==R.id.down_tv && !left_tv.isEnabled())
//					){
//						return true;
//					}
//				}else if(keyCode==KeyEvent.KEYCODE_DPAD_RIGHT){
//					if(v.getId()==R.id.right_tv
//					  ||(v.getId()==R.id.center_tv && !right_tv.isEnabled())
//					  ||(v.getId()==R.id.left_tv && !center_tv.isEnabled() && !right_tv.isEnabled())
//					  ||(v.getId()==R.id.up_tv && !right_tv.isEnabled())
//					  ||(v.getId()==R.id.down_tv && !right_tv.isEnabled())
//					){
//						return true;
//					}
//				}else if(keyCode==KeyEvent.KEYCODE_DPAD_DOWN){
//					if(v.getId()==R.id.down_tv
//					  ||(v.getId()==R.id.center_tv && !down_tv.isEnabled())
//					  ||(v.getId()==R.id.up_tv && !center_tv.isEnabled() && !down_tv.isEnabled())
//					  ||(v.getId()==R.id.left_tv && !down_tv.isEnabled())
//					  ||(v.getId()==R.id.right_tv && !down_tv.isEnabled())
//					){
//						return true;
//					}
//			}else
				if(keyCode==KeyEvent.KEYCODE_BACK){
					if(item_dialog_framLyt.getVisibility()==View.VISIBLE){
						item_dialog_framLyt.setVisibility(View.GONE);
						if(showItemDialogBeforView!=null)showItemDialogBeforView.requestFocus();
						setVisiable();
						return true;
					}
				}
//			}
			
			return false;
		}
	};
	View.OnKeyListener onnumkeyListener=new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if(keyCode==KeyEvent.KEYCODE_DPAD_CENTER||keyCode==KeyEvent.KEYCODE_ENTER){
		
					String itemTag = v.getTag().toString();
					tagDistribute(v,itemTag);
			}
			return false;
		}
	};
	/**
	 * 
	 */
	View.OnClickListener onclickListener= new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			String itemTag;
			switch (v.getId()) {
			case R.id.dialNum1:
				itemTag= v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum2:
				dialNum2.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum3:
				dialNum3.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum4:
				dialNum4.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum5:
				dialNum5.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum6:
				dialNum6.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum7:
				dialNum7.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum8:
				dialNum8.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum9:
				dialNum9.setVisibility(View.INVISIBLE);
				itemTag = v.getTag().toString();
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum11:
			case R.id.bihua_num1:	
			case R.id.bihua_num2:	
			case R.id.bihua_num3:	
			case R.id.bihua_num4:	
			case R.id.bihua_num5:	
			case R.id.bihua_num6:	
			case R.id.bihua_num8:	
				itemTag = v.getTag().toString();
				
				tagDistribute(v,itemTag);
				break;
			case R.id.dialNum10:
			case R.id.bihua_num7:
				if(callBackListener!=null){
					callBackListener.onClearClicked();
				}
				break;
			case R.id.dialNum12:
			case R.id.bihua_num9:
				if(callBackListener!=null){
					callBackListener.onDeleteClicked();
				}
				break;
			case R.id.dialNum14:
				inputManager.showSoftInput(v, 0);
				et.requestFocus();
				break;
				
			case R.id.up_tv:
			case R.id.down_tv:
			case R.id.left_tv:
			case R.id.center_tv:
			case R.id.right_tv:
				if(item_dialog_framLyt.getVisibility()!=View.GONE){
					item_dialog_framLyt.setVisibility(View.GONE);
				}
				dispachChar(((TextView)v).getText().charAt(0));
				setVisiable();
				if(showItemDialogBeforView!=null)showItemDialogBeforView.requestFocus();  //resume the focus
				break;
			default:
				break;
			}
			
		}
	};
	
	CompoundButton.OnCheckedChangeListener rdBtnCheckChangeListener = new OnCheckedChangeListener() {
		

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			
			if(isChecked){
				currentTypeTag = buttonView.getTag().toString();
				Log.i("guoanDemo", "------currentTypeTag---"+currentTypeTag);
				switch (buttonView.getId()) {
//				case R.id.t9_rd_btn:
				case R.id.dialNum10:
					showFramLayoutItemByIndex(1);
					break;
	
				default:
					break;
				}
			}
		}
	};

	private void showFramLayoutItemByIndex(int index){
		if(content_etv!=null && index<content_etv.getChildCount()){
			content_etv.getChildCount();
			for (int i = 0; i < content_etv.getChildCount(); i++) {
				if(i==index){
					content_etv.getChildAt(i).setVisibility(View.VISIBLE);
				}else{
					content_etv.getChildAt(i).setVisibility(View.GONE);
				}
			}
		}
	}
	
	private void tagDistribute(View view,String itemTag){
		try{
			if(!TextUtils.isEmpty(itemTag)){
				int itemIndex = Integer.valueOf(itemTag)-1; //tag index is from 0 begin;
				//t9
				if(currentTypeTag.equalsIgnoreCase("t9") && itemIndex<pinYingStrArr.length){
					char [] itemChars = pinYingStrArr[itemIndex];
					if(itemChars.length>0){
						dispachChar(itemChars[0]);
						return ;
					}
				}else if(currentTypeTag.equalsIgnoreCase("pinyin") && itemIndex<pinYingStrArr.length){
					char [] itemChars = pinYingStrArr[itemIndex];
					if(itemChars.length==1){
						dispachChar(itemChars[0]);
						return;
					}else if(itemChars.length>1){
						showItemDialog(view,itemChars);
					}
					
				}else if(currentTypeTag.equalsIgnoreCase("bihua") && itemIndex<biHua.length){
					char [] itemChars = biHua[itemIndex];
					if(itemChars.length==1){
						dispachChar(itemChars[0]);
						return;
					}else if(itemChars.length>1){
						showItemDialog(view,itemChars);
					}
				}
			}
			
		}catch (Exception e) {
		}
	}
	
	private void showItemDialog(View view,char[] itemChars){
		DisplayMetrics metric = new DisplayMetrics(); 
        metric=getResources().getDisplayMetrics();
        float density=metric.density;
        LogUtil.i("guoanDemo", "density is:------"+density);
		int rowIndex =0;
		int cloumnIndex=0;
		
		String viewTag = (String)view.getTag();
		try{
			int viewIndex = Integer.valueOf(viewTag)-1;
			LogUtil.i("guoanDemo", "viewIndex is:------"+viewIndex);
			cloumnIndex = viewIndex%3; //cloumn index number
			LogUtil.i("guoanDemo", "cloumnIndex is:------"+cloumnIndex);

			rowIndex = viewIndex/3;		//row index number
			LogUtil.i("guoanDemo", "rowIndex is:------"+rowIndex);
		}catch (Exception e) {
		}
		
		item_dialog_framLyt.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

		float init_x = content_etv.getX();
		float init_y = content_etv.getY();
		
		float Pos_x = cloumnIndex * (view.getMeasuredWidth()+4*density);
		LogUtil.i("guoandemo", "Pos_x is :----------"+Pos_x);
		LogUtil.i("guoandemo", "view.getMeasuredWidth() is :----------"+view.getMeasuredWidth());
		int half_w = view.getMeasuredWidth()/2;
		LogUtil.i("guoandemo", "half_w is :----------"+half_w);

		int item_half_w = item_dialog_framLyt.getMeasuredWidth()/2;
		LogUtil.i("guoandemo", "item_half_w is :----------"+item_half_w);
		float center_X = Pos_x + half_w;
		LogUtil.i("guoandemo", "center_X is :----------"+center_X);
		float px = center_X -item_half_w-density;
		LogUtil.i("guoandemo", "px is :----------"+px);
		float Pos_y = rowIndex *( view.getMeasuredHeight()+4*density);
		int half_h = view.getMeasuredHeight()/2;
		int item_half_h = item_dialog_framLyt.getMeasuredHeight()/4;
		
		float center_Y = Pos_y + half_h;
		float py = center_Y - item_half_h;
		LogUtil.i("guoandemo", "py is :----------"+py);
		item_dialog_framLyt.setX(init_x+px);
		item_dialog_framLyt.setY(init_y+Pos_y);
		item_dialog_framLyt.setVisibility(View.VISIBLE);
		down_tv.setVisibility(View.INVISIBLE);
		if(itemChars.length>0){
//			up_tv.setEnabled(true);
//			up_tv.setText(String.valueOf(itemChars[0]));
			left_tv.setEnabled(true);
			left_tv.setText(String.valueOf(itemChars[0]));
		}else {
			left_tv.setText("");
			left_tv.setEnabled(false);
		}
		
		if(itemChars.length>1){
			right_tv.setEnabled(true);
			right_tv.setText(String.valueOf(itemChars[1]));
//			left_tv.setEnabled(true);
//			left_tv.setText(String.valueOf(itemChars[1]));
		}else {
			right_tv.setText("");
			right_tv.setEnabled(false);
		}
		
		if(itemChars.length>2){
			up_tv.setEnabled(true);
			up_tv.setText(String.valueOf(itemChars[2]));
//			center_tv.setEnabled(true);
//			center_tv.setText(String.valueOf(itemChars[2]));
		}else {
			up_tv.setText("");
			up_tv.setEnabled(false);
		}
		
		if(itemChars.length>3){

			center_tv.setEnabled(true);
			center_tv.setText(String.valueOf(itemChars[3]));
//			right_tv.setEnabled(true);
//			right_tv.setText(String.valueOf(itemChars[3]));
		}else {
			center_tv.setText("");
			center_tv.setEnabled(false);
		}
		
		if(itemChars.length>4){
			down_tv.setEnabled(true);
			down_tv.setVisibility(View.VISIBLE);

			down_tv.setText(String.valueOf(itemChars[4]));
		}else {
			down_tv.setText("");
			down_tv.setEnabled(false);
		}
		
		up_tv.requestFocus();
		showItemDialogBeforView = view;
	}
	
	/**
	 * dispach selected char to listener
	 * @param ch
	 */
	private void dispachChar(char ch){
		if(callBackListener!=null){
			callBackListener.onCharSelected(ch);
		}
	}
	
	public interface CallBackListener{
		/**
		 * on deleted a char
		 */
		public void onDeleteClicked();
		/**
		 * long click delete btn,should be clear the chars
		 */
		public void onDeleteLongClicked();
		/**
		 * clear btn clicked
		 */
		public void onClearClicked();
		/**
		 * selected a char, this char should be append the view's content
		 * @param c
		 */
		public void onCharSelected(Character c);

	}

	private void setVisiable(){
		dialNum1.setVisibility(View.VISIBLE);
		dialNum2.setVisibility(View.VISIBLE);
		dialNum3.setVisibility(View.VISIBLE);
		dialNum4.setVisibility(View.VISIBLE);
		dialNum5.setVisibility(View.VISIBLE);
		dialNum6.setVisibility(View.VISIBLE);
		dialNum7.setVisibility(View.VISIBLE);
		dialNum8.setVisibility(View.VISIBLE);
		dialNum9.setVisibility(View.VISIBLE);

	}
	
}
