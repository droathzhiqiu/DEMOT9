package com.lcw.comm.fragment;

import android.util.Log;

public class LogUtil {
	private final static String TAG = "TX_LOG";
	public static boolean isDebug=true;
	public static void e(String tag,String msg,Exception e)
	{
		if(!isDebug){
			return;
		}
		StackTraceElement invoke=getInvoker();
		Log.e(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	public static void i(String tag,String msg)
	{	if(!isDebug){
		return;
	}
		StackTraceElement invoke=getInvoker();
		Log.i(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	public static void v(String tag,String msg)
	{
		if(!isDebug){
			return;
		}
		StackTraceElement invoke=getInvoker();
		Log.v(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	public static void d(String tag,String msg)
	{
		if(!isDebug){
			return;
		}
		StackTraceElement invoke=getInvoker();
		Log.d(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	public static void w(String tag,String msg)
	{
		if(!isDebug){
			return;
		}
		StackTraceElement invoke=getInvoker();
		Log.w(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	public static void e(String tag,String msg)
	{
		if(!isDebug){
			return;
		}
		StackTraceElement invoke=getInvoker();
		Log.e(invoke.getClassName(),"【"+invoke.getMethodName()+":"+invoke.getLineNumber()+"】"+ msg);
	}
	private static StackTraceElement getInvoker(){
		return Thread.currentThread().getStackTrace()[4];
	}

}
